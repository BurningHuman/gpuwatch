A simple bash script to monitor GPU values delivered from the AMDGPU driver.

Use it at your own risk!

Syntax as follows:

./gpuwatch "Interval" "card*" "hwmon*" "store directory"

Where "card*" is the card1 name assigned by the AMDGPU driver, which you can find over the directory /sys/class/drm/ and there select one of your installed cards.

And since every card comes with a different number of "hwmon"s. You can chose which one you want to monitor with the same method used above.

Example of usage:

./gpuwatch.sh "0.5" "card1" "hwmon1" "~/"

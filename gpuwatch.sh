INTERVAL=$1
HWMON=$3
CARD="/sys/class/drm/"$2"/device/hwmon/$HWMON/"
STORE=$4
watch -n$INTERVAL \
	"cat $CARD"freq1_input" >> $STORE"gpu_fan1" \
	&& cat $CARD"power1_input" >> $STORE"gpu_power" \
	&& cat $CARD"in0_input" >> $STORE"gpu_voltage" "
